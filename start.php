<?php 

		session_start();

	define('ENVIRONMENT', 'development');

	if (defined('ENVIRONMENT')){

		switch (ENVIRONMENT){
			case 'development':
				error_reporting(E_ALL);
			break;
		
			case 'testing':
			case 'production':
				error_reporting(0);
			break;

			default:
				exit('The application environment is not set correctly.');
		}
	}
	define('BASE_PATH',dirname(__FILE__));
	define('CONTROLLER_PATH',dirname(__FILE__).DIRECTORY_SEPARATOR.'actions');
	define('CLASS_PATH',dirname(__FILE__).DIRECTORY_SEPARATOR.'classes');
	define('VIEW_PATH',dirname(__FILE__).DIRECTORY_SEPARATOR.'views');
	define('ASSETS_PATH',dirname(__FILE__).DIRECTORY_SEPARATOR.'assets');

	define('UPLOAD_PATH',dirname(__FILE__)."/uploads/");

	define('BASE_URL','http://localhost/scaleengine/');

	require_once('vendor/autoload.php');
	require_once('classes/autoload.php');

	$db=Db::connect();
	

		
	
	
	$loader = new Twig_Loader_Filesystem('views');

	$loader->addPath('views/layouts', 'layouts');
	$loader->addPath('views/backend', 'backend');
	$loader->addPath('views/frontend', 'frontend');

	$twig = new Twig_Environment($loader, array(
	    'cache' => 'tmp/cache',
	    'debug' => true,
	    'auto_reload' => true
	));
	$loggedIn=true;


/*
	if(isset($_GET['page'])):
		if( $_GET['page']=='' || $_GET['page']=='index' || $_GET['page']=='index.php' ){
			include_once 'start.php';
		}else{
			if(isset($_GET['id'])){
				$id=$_GET['id'];
				include_once CONTROLLER_PATH.DIRECTORY_SEPARATOR.$_GET['page'].'.php';	
			}else{
				include_once CONTROLLER_PATH.DIRECTORY_SEPARATOR.$_GET['page'].'.php';		
			}
			
		}
	else:
			include_once 'start.php';
	endif;		
	
	*/
	
?>