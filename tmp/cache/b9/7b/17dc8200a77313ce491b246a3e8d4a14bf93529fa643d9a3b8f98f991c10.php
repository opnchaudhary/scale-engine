<?php

/* @backend/index.twig.html */
class __TwigTemplate_b97b17dc8200a77313ce491b246a3e8d4a14bf93529fa643d9a3b8f98f991c10 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("@layouts/backend.twig.html");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@layouts/backend.twig.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Main Index Page";
    }

    // line 5
    public function block_head($context, array $blocks = array())
    {
        // line 6
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
    <style type=\"text/css\">
        .important { color: #336699; }
    </style>
";
    }

    // line 12
    public function block_content($context, array $blocks = array())
    {
        // line 13
        echo "    
\t\t\t\t\t\t\t\t\t
\t
\t<div class=\"row\">
\t\t\t<h1>Dashboard</h1>
\t\t\t<p>Welcome FirstName LastName,</p>
\t\t\t<p>You are logged in as an administrator user. Feel free to surf.</p>\t
\t</div>

";
    }

    public function getTemplateName()
    {
        return "@backend/index.twig.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 13,  49 => 12,  39 => 6,  36 => 5,  30 => 3,);
    }
}
