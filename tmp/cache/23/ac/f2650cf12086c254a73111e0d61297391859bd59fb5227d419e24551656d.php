<?php

/* @layouts/backend.twig.html */
class __TwigTemplate_23acf2650cf12086c254a73111e0d61297391859bd59fb5227d419e24551656d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html>
    <head>

    \t";
        // line 5
        $this->displayBlock('head', $context, $blocks);
        // line 10
        echo "
    </head>
    <body>

    \t<div class=\"navbar navbar-default navbar-fixed-top\" role=\"navigation\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"navbar-header\">
\t\t\t\t\t<button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
            \t\t\t<span class=\"sr-only\">Toggle navigation</span>
\t\t\t\t\t    <span class=\"icon-bar\"></span>
\t\t\t\t\t    <span class=\"icon-bar\"></span>
\t\t\t\t\t    <span class=\"icon-bar\"></span>
          \t\t\t</button>
          \t\t\t<a class=\"navbar-brand\" href=\"";
        // line 23
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "\">Admin</a>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"navbar-collapse collapse\">
\t\t\t\t\t\t<ul class=\"nav navbar-nav\">
\t\t\t\t\t\t\t<li class=\"active\">
\t\t\t\t\t\t\t\t<a href=\"index.php\">
\t\t\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-home\"></span> Dashboard
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li class=\"dropdown\">
\t\t\t\t\t\t\t\t<a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
\t\t\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-film\"></span> Videos <span class=\"caret\"></span>
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<ul class=\"dropdown-menu\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 38
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "videos\"><span class=\"glyphicon glyphicon-film\"></span>List Videos</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 41
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "video_categories\"><span class=\"glyphicon glyphicon-user\"></span>Video Categories</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a href=\"http://localhost/newcrm/public/admin/todos\">
\t\t\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-list-alt\"></span> Todos
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a href=\"";
        // line 51
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "configuration\">
\t\t\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-cog\"></span>Configuration
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<ul class=\"nav navbar-nav navbar-right\">
\t\t\t\t\t\t\t\t<li>\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<a href=\"actions/logout.php\">
\t\t\t\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-log-out\"></span> Logout
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t</div><!--/.navbar-collapse -->
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>\t\t

\t\t<div class=\"container\" style=\"margin-top:35px\">
        
\t        ";
        // line 70
        $this->displayBlock('content', $context, $blocks);
        // line 72
        echo "
    \t</div>

    \t<footer>
    \t</footer>
    \t<script src=\"assets/js/jquery.min.js\"></script> 
       <script src=\"assets/js/bootstrap.min.js\"></script> 

    </body>
</html>";
    }

    // line 5
    public function block_head($context, array $blocks = array())
    {
        echo "            
            <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
            <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "assets/css/bootstrap.min.css\">
\t\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "assets/css/bootstrap-theme.min.css\">
        ";
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
    }

    // line 70
    public function block_content($context, array $blocks = array())
    {
        // line 71
        echo "\t        ";
    }

    public function getTemplateName()
    {
        return "@layouts/backend.twig.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  146 => 71,  143 => 70,  138 => 6,  132 => 8,  128 => 7,  124 => 6,  119 => 5,  106 => 72,  104 => 70,  82 => 51,  69 => 41,  63 => 38,  45 => 23,  30 => 10,  28 => 5,  22 => 1,);
    }
}
