<?php

/* @backend/create_category.twig.html */
class __TwigTemplate_5f8c5177cead2c3bd20ee8d786c1f9a406f27f1c9711a515641f76f6b96961cf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("@layouts/backend.twig.html");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@layouts/backend.twig.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Main Index Page";
    }

    // line 5
    public function block_head($context, array $blocks = array())
    {
        // line 6
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
    <style type=\"text/css\">
        .important { color: #336699; }
    </style>
";
    }

    // line 12
    public function block_content($context, array $blocks = array())
    {
        // line 13
        echo "    
\t\t\t\t\t\t\t\t\t
\t
\t<div class=\"row\" style=\"margin-top:30px;\">
\t\t\t";
        // line 17
        if (((isset($context["success_msg"]) ? $context["success_msg"] : null) != "")) {
            // line 18
            echo "\t\t\t";
            echo twig_escape_filter($this->env, (isset($context["success_msg"]) ? $context["success_msg"] : null), "html", null, true);
            echo "
\t\t\t";
        }
        // line 20
        echo "\t\t\t
\t\t\t<form method=\"post\" action=\"\"  enctype=\"multipart/form-data\">

\t\t\t\t<input type=\"hidden\" name=\"csrf_token\" value=\"";
        // line 23
        echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : null), "html", null, true);
        echo "\" />

\t\t\t\t<!-- Category Title -->
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"control-label\" for=\"categoryTitle\">Category Title</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"category_title\" id=\"categoryTitle\" value=\"\" required />\t\t\t\t\t
\t\t\t\t</div>

\t\t\t\t<!-- Category Description -->
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"control-label\" for=\"categoryDescription\">Category Description</label>\t\t\t\t\t
\t\t\t\t\t<textarea class=\"form-control\" name=\"category_description\" id=\"categoryDescription\"> 
\t\t\t\t\t</textarea>\t\t
\t\t\t\t</div>

\t\t\t

\t\t\t\t<!-- Category Image -->
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"control-label\" for=\"categoryImage\">Category Image</label>\t\t\t\t\t
\t\t\t\t\t<input  class=\"form-control\"  type=\"file\" id=\"categoryImage\" name=\"category_image\"/>
\t\t\t\t</div>

\t\t\t\t<!--Cateogry Status -->
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"control-label\" for=\"categoryStatus\">Category Status</label>\t\t\t\t\t
\t\t\t\t\t<select class=\"form-control\" name=\"category_status\" id=\"categoryStatus\">
\t\t\t\t\t\t<option value=\"1\">Visible</option>
\t\t\t\t\t\t<option value=\"0\">Invisible</option>
\t\t\t\t\t</select>
\t\t\t\t</div>

\t\t\t\t

\t\t\t\t<!-- Form actions -->
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<button type=\"submit\" class=\"btn btn-primary\">Save</button>\t\t\t\t\t
\t\t\t\t\t<button type=\"reset\" class=\"btn btn-default\">Reset</button>
\t\t\t\t</div>

\t\t\t</form>
\t</div>

";
    }

    public function getTemplateName()
    {
        return "@backend/create_category.twig.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 23,  66 => 20,  60 => 18,  58 => 17,  52 => 13,  49 => 12,  39 => 6,  36 => 5,  30 => 3,);
    }
}
