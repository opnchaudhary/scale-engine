<?php

/* @backend/video_upload.twig.html */
class __TwigTemplate_2c52ccdb913d1e6006a2f56dfb4272bfb6ef76caa8fa0ba75c3451d1acfdbd5e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("@layouts/backend.twig.html");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@layouts/backend.twig.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Main Index Page";
    }

    // line 5
    public function block_head($context, array $blocks = array())
    {
        // line 6
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
    <style type=\"text/css\">
        .important { color: #336699; }
    </style>
";
    }

    // line 12
    public function block_content($context, array $blocks = array())
    {
        // line 13
        echo "    
\t\t\t\t\t\t\t\t\t
\t
\t<div class=\"row\" style=\"margin-top:30px;\">
\t\t\t<form method=\"post\" action=\"\">

\t\t\t\t<input type=\"hidden\" name=\"csrf_token\" value=\"";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : null), "html", null, true);
        echo "\" />

\t\t\t\t<!-- Video Title -->
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"control-label\" for=\"videoTitle\">Video Title</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"video_title\" id=\"videoTitle\" value=\"\" required />\t\t\t\t\t
\t\t\t\t</div>

\t\t\t\t<!-- Video Description -->
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"control-label\" for=\"videoDescription\">Video Description</label>\t\t\t\t\t
\t\t\t\t\t<textarea class=\"form-control\" name=\"video_description\" id=\"videoDescription\"> 
\t\t\t\t\t</textarea>\t\t
\t\t\t\t</div>

\t\t\t\t<!-- Video File -->
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"control-label\" for=\"videoFile\">Video File</label>\t\t\t\t\t
\t\t\t\t\t<input  class=\"form-control\"  type=\"file\" id=\"videoFile\" name=\"video_file\"/>
\t\t\t\t</div>

\t\t\t\t<!-- Video Image -->
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"control-label\" for=\"videoImage\">Video Image</label>\t\t\t\t\t
\t\t\t\t\t<input  class=\"form-control\"  type=\"file\" id=\"videoImage\" name=\"video_image\"/>
\t\t\t\t</div>

\t\t\t\t<!-- -->
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"control-label\" for=\"videoStatu\">Video Status</label>\t\t\t\t\t
\t\t\t\t\t<select class=\"form-control\" name=\"video_status\" id=\"videoStatus\">
\t\t\t\t\t\t<option value=\"1\">Visible</option>
\t\t\t\t\t\t<option value=\"0\">Invisible</option>
\t\t\t\t\t</select>
\t\t\t\t</div>

\t\t\t\t

\t\t\t\t<!-- Form actions -->
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<button type=\"submit\" class=\"btn btn-primary\">Upload</button>\t\t\t\t\t
\t\t\t\t\t<button type=\"reset\" class=\"btn btn-default\">Reset</button>
\t\t\t\t</div>

\t\t\t</form>
\t</div>

";
    }

    public function getTemplateName()
    {
        return "@backend/video_upload.twig.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 19,  52 => 13,  49 => 12,  39 => 6,  36 => 5,  30 => 3,);
    }
}
