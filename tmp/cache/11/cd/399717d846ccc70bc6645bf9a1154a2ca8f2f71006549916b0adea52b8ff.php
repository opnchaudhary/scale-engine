<?php

/* @backend/login.twig.html */
class __TwigTemplate_11cd399717d846ccc70bc6645bf9a1154a2ca8f2f71006549916b0adea52b8ff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("@layouts/backend.twig.html");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@layouts/backend.twig.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Main Index Page";
    }

    // line 5
    public function block_head($context, array $blocks = array())
    {
        // line 6
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
    <style type=\"text/css\">
        .important { color: #336699; }
    </style>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"assets/css/bootstrap.min.css\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"assets/css/bootstrap-theme.min.css\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"assets/backend/css/login.css\">
";
    }

    // line 15
    public function block_content($context, array $blocks = array())
    {
        // line 16
        echo "    <form method=\"post\" action=\"http://utouchpos.com/tools/public/admin/signin\" class=\"form-signin\" role=\"form\">\t\t
\t\t<h3>Please signin</h3>
\t\t<input type=\"hidden\" name=\"_token\" value=\"C923uSi8igfEyrK2OxzRTNyD1dafD3QYVOyi9OQ0\" />
\t\t<div class=\"form-group\">
\t\t\t<input class=\"form-control\" type=\"email\" name=\"email\" id=\"email\" value=\"\" placeholder=\"Email address\" required autofocus />
\t\t\t\t</div>\t\t
\t\t<div class=\"form-group\">
\t\t\t<input type=\"password\" class=\"form-control\" placeholder=\"Password\" name=\"password\" id=\"password\" value=\"\" required />
\t\t\t\t</div>
\t\t<button class=\"btn btn-lg btn-primary btn-block\" type=\"submit\">Sign in</button>
\t</form>

";
    }

    public function getTemplateName()
    {
        return "@backend/login.twig.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 16,  52 => 15,  39 => 6,  36 => 5,  30 => 3,);
    }
}
