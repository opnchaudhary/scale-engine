<?php

/* @backend/video_categories.twig.html */
class __TwigTemplate_27e0cd709cc71f257c559e40209c4768f932c8ac4d5ae959d3fc2f2677cc4a76 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("@layouts/backend.twig.html");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@layouts/backend.twig.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Main Index Page";
    }

    // line 5
    public function block_head($context, array $blocks = array())
    {
        // line 6
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
    <style type=\"text/css\">
        .important { color: #336699; }
    </style>
";
    }

    // line 12
    public function block_content($context, array $blocks = array())
    {
        // line 13
        echo "    
\t\t\t\t\t\t\t\t\t
\t
\t<div class=\"row\" style=\"margin-top:30px;\">

\t\t<div class=\"btn-group\">
\t\t\t<a href=\"";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "create_category\" class=\"btn btn-default\">Create Category</a>  \t\t\t\t
\t\t</div>

\t\t<table class=\"table table-bordered table-hover\">

\t\t\t<thead>
\t\t\t\t<th>S.No.</th>
\t\t\t\t<th>Title</th>
\t\t\t\t<th>Description</th>
\t\t\t\t<th>Image</th>
\t\t\t\t<th>Status</th>
\t\t\t\t<th>Actions</th>
\t\t\t</thead>

\t\t\t<tbody>
\t\t\t\t";
        // line 34
        $context["counter"] = 0;
        // line 35
        echo "\t\t\t\t";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["video_categories"]) ? $context["video_categories"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 36
            echo "\t\t\t\t";
            $context["counter"] = ((isset($context["counter"]) ? $context["counter"] : null) + 1);
            // line 37
            echo "\t\t\t\t<tr>
\t\t\t\t\t<td>";
            // line 38
            echo twig_escape_filter($this->env, (isset($context["counter"]) ? $context["counter"] : null), "html", null, true);
            echo "</td>
\t\t\t        <td>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category"]) ? $context["category"] : null), "title"));
            echo "</td>
\t\t\t        <td>";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category"]) ? $context["category"] : null), "description"));
            echo "</td>
\t\t\t        <td>";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category"]) ? $context["category"] : null), "image"), "html", null, true);
            echo "</td>
\t\t\t        <td>
\t\t\t        \t";
            // line 43
            if (($this->getAttribute((isset($context["category"]) ? $context["category"] : null), "status") == "1")) {
                // line 44
                echo "\t\t\t        \tVisible
\t\t\t        \t";
            } else {
                // line 46
                echo "\t\t\t        \tInvisible
\t\t\t        \t";
            }
            // line 48
            echo "\t\t\t        </td>
\t\t\t        <td>
\t\t\t        \t<a href=\"";
            // line 50
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "edit_category?id=";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category"]) ? $context["category"] : null), "id"), "html", null, true);
            echo "\" class=\"btn btn-default btn-sm\"><span class=\"glyphicon glyphicon-edit\"></span> Edit</a>
\t\t\t        \t<a href=\"";
            // line 51
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "delete_category?id=";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category"]) ? $context["category"] : null), "id"), "html", null, true);
            echo "\" class=\"btn btn-default btn-sm\"><span class=\"glyphicon glyphicon-trash\"></span> Delete</a>
\t\t\t        \t
\t\t\t        </td>
\t\t\t    </tr>
\t\t\t    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "\t\t\t</tbody>

\t\t\t
\t\t</table>
\t</div>

";
    }

    public function getTemplateName()
    {
        return "@backend/video_categories.twig.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  141 => 56,  128 => 51,  122 => 50,  118 => 48,  114 => 46,  110 => 44,  108 => 43,  103 => 41,  99 => 40,  95 => 39,  91 => 38,  88 => 37,  85 => 36,  80 => 35,  78 => 34,  60 => 19,  52 => 13,  49 => 12,  39 => 6,  36 => 5,  30 => 3,);
    }
}
