<?php

/* @backend/config.twig.html */
class __TwigTemplate_15a8da5f995f1217cd0e88bad06b12203465adbc7bb4aba3cd0d42870fecf5e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("@layouts/backend.twig.html");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@layouts/backend.twig.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Main Index Page";
    }

    // line 5
    public function block_head($context, array $blocks = array())
    {
        // line 6
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
    <style type=\"text/css\">
        .important { color: #336699; }
    </style>
";
    }

    // line 12
    public function block_content($context, array $blocks = array())
    {
        // line 13
        echo "    
\t\t\t\t\t\t\t\t\t
\t
\t<div class=\"row\" style=\"margin-top:30px;\">
\t\t\t<form method=\"post\" action=\"\">

\t\t\t\t<input type=\"hidden\" name=\"csrf_token\" value=\"";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : null), "html", null, true);
        echo "\" />

\t\t\t\t<!-- API Key -->
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"control-label\" for=\"apiKey\">API Key</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"api_key\" id=\"apiKey\" value=\"\" required />\t\t\t\t\t
\t\t\t\t</div>

\t\t\t\t<!-- API Secret -->
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"control-label\" for=\"apiSecret\">API Secret</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"api_secret\" id=\"apiSecret\" value=\"\" required />\t\t\t\t\t
\t\t\t\t</div>

\t\t\t\t<!-- API Url -->
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"control-label\" for=\"apiUrl\">API Url</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"api_url\" id=\"apiUrl\" value=\"\" required />\t\t\t\t\t
\t\t\t\t</div>

\t\t\t\t<!-- CDN ID -->
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"control-label\" for=\"cdnId\">CDN ID</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"cdn_id\" id=\"cdnId\" value=\"\" required />\t\t\t\t\t
\t\t\t\t</div>

\t\t\t\t<!-- FTP User -->
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"control-label\" for=\"ftpUser\">FTP User</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"ftp_user\" id=\"ftpUser\" value=\"\" required />\t\t\t\t\t
\t\t\t\t</div>

\t\t\t\t<!-- FTP Host -->
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"control-label\" for=\"ftpHost\">FTP Host</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"ftp_host\" id=\"ftpHost\" value=\"\" required />\t\t\t\t\t
\t\t\t\t</div>

\t\t\t\t<!-- FTP Password -->
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"control-label\" for=\"ftpPassword\">FTP Password</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"ftp_password\" id=\"ftpPassword\" value=\"\" required />\t\t\t\t\t
\t\t\t\t</div>

\t\t\t\t<!-- Form actions -->
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<button type=\"submit\" class=\"btn btn-primary\">Save</button>\t\t\t\t\t
\t\t\t\t\t<button type=\"reset\" class=\"btn btn-default\">Reset</button>
\t\t\t\t</div>

\t\t\t</form>
\t</div>

";
    }

    public function getTemplateName()
    {
        return "@backend/config.twig.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 19,  52 => 13,  49 => 12,  39 => 6,  36 => 5,  30 => 3,);
    }
}
