<?php

/* @backend/videos.twig.html */
class __TwigTemplate_73e124212ce9e6188c8087aa205b2b1d02115c06c312fac5f4188b8538070839 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("@layouts/backend.twig.html");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@layouts/backend.twig.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Main Index Page";
    }

    // line 5
    public function block_head($context, array $blocks = array())
    {
        // line 6
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
    <style type=\"text/css\">
        .important { color: #336699; }
    </style>
";
    }

    // line 12
    public function block_content($context, array $blocks = array())
    {
        // line 13
        echo "    
\t\t\t\t\t\t\t\t\t
\t
\t<div class=\"row\" style=\"margin-top:30px;\">
\t\t<div class=\"btn-group\">
\t\t\t<a href=\"";
        // line 18
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "video_upload\" class=\"btn btn-default\">Video Upload</a>  \t\t\t\t
\t\t</div>
\t\t<table class=\"table table-bordered table-hover\">

\t\t\t<thead>
\t\t\t\t<th>S.No.</th>
\t\t\t\t<th>Title</th>
\t\t\t\t<th>Description</th>
\t\t\t\t<th>Image</th>
\t\t\t\t<th>Status</th>
\t\t\t</thead>

\t\t\t<tbody>
\t\t\t\t";
        // line 31
        $context["counter"] = 0;
        // line 32
        echo "\t\t\t\t";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["videos"]) ? $context["videos"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["video"]) {
            // line 33
            echo "\t\t\t\t";
            $context["counter"] = ((isset($context["counter"]) ? $context["counter"] : null) + 1);
            // line 34
            echo "\t\t\t\t<tr>
\t\t\t\t\t<td>";
            // line 35
            echo twig_escape_filter($this->env, (isset($context["counter"]) ? $context["counter"] : null), "html", null, true);
            echo "</td>
\t\t\t        <td>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["video"]) ? $context["video"] : null), "title"));
            echo "</td>
\t\t\t        <td>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["video"]) ? $context["video"] : null), "description"));
            echo "</td>
\t\t\t        <td>";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["video"]) ? $context["video"] : null), "image"), "html", null, true);
            echo "</td>
\t\t\t        <td>
\t\t\t        \t";
            // line 40
            if (($this->getAttribute((isset($context["video"]) ? $context["video"] : null), "status") == "1")) {
                // line 41
                echo "\t\t\t        \tVisible
\t\t\t        \t";
            } else {
                // line 43
                echo "\t\t\t        \tInvisible
\t\t\t        \t";
            }
            // line 45
            echo "\t\t\t        </td>
\t\t\t    </tr>
\t\t\t    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['video'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "\t\t\t</tbody>

\t\t\t
\t\t</table>
\t</div>

";
    }

    public function getTemplateName()
    {
        return "@backend/videos.twig.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 48,  115 => 45,  111 => 43,  107 => 41,  105 => 40,  100 => 38,  96 => 37,  92 => 36,  88 => 35,  85 => 34,  82 => 33,  77 => 32,  75 => 31,  59 => 18,  52 => 13,  49 => 12,  39 => 6,  36 => 5,  30 => 3,);
    }
}
