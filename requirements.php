<!doctype html>
<html>

	<head>

		<meta charset="utf-8" />
		<title>Requirements Check</title>
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.min.css">
	</head>

	<body>
		<div class="container">
			<div class="page-header">
				<h3>Requirements Check</h3>
			</div>


			<div class="row margin-top-20">
				<div class="col-xs-12">
					<table class="table table-bordered table-hover">

						<thead>
							<th>Utility</th>
							<th>Available</th>
							<th>Path</th>
						</thead>

						<tbody>
							<tr>
								<td>
									FFMPEG
								</td>
								<td>
									<?php 
										$ffmpeg = trim(shell_exec('type -P ffmpeg'));
										if (empty($ffmpeg)){
										    echo "<font color='red'>Not Available</font><br />";
										}else{
											echo "<font color='green'>Available</font><br />";
										}
									?>
								</td>
								<td>
									<?php
									if (!empty($ffmpeg)){
									echo $ffmpeg;
									}
									?>
								</td>
							</tr>

							<tr>
								<td> PDO MySQL</td>
								<td>
									<?php 
									if (!defined('PDO::ATTR_DRIVER_NAME')) {
										echo "<font color='red'>Unavailable</font><br />";
									}elseif(defined('PDO::ATTR_DRIVER_NAME')){
										echo "<font color='green'>Available</font><br />";
									}
									?>
								</td>
								<td>
								</td>
							</tr>

								
						</tbody>
					</table>
				</div>
			</div>
		</div>

	</body>
</html>
