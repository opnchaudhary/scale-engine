<?php 
/**
* @category class
* @author Paras Nath Chaudhary
* @version 1.0
*/

/**
* class Video
*/

class Video{
	
	/**
	* Adds videos
	* @param
	* @return 
	*/
	public function addVideo(){

	}

	/**
	* Function to get videos
	* @param int $limit, int $offset
	* @return videos
	*/
	public static function getVideos($limit=10,$offset=0){

		global $db;		
		$videos = $db->prepare("SELECT * FROM  `videos` order by timestamp desc LIMIT :offset , :limit");
		$videos->bindParam(':offset',$offset,PDO::PARAM_INT);
		$videos->bindParam(':limit',$limit,PDO::PARAM_INT);
		
		$videos->execute();

		return  $videos->fetchAll();	
	}
}
?>