<?php 
class Session{
	
	public static function flash( $name = '', $message = '', $class = 'success fadeout-message' ){
    //We can only do something if the name isn't empty

    if( !empty( $name ) ){
        //No message, create it
        if( !empty( $message ) && empty( $_SESSION[$name] ) ){

            if( !empty( $_SESSION[$name] ) ){
                unset( $_SESSION[$name] );
            }

            if( !empty( $_SESSION[$name.'_class'] ) ){
                unset( $_SESSION[$name.'_class'] );
            }
 
            $_SESSION[$name] = $message;
            $_SESSION[$name.'_class'] = $class;

        }elseif( !empty( $_SESSION[$name] ) && empty( $message ) ){ //Message exists, display it

            $class = !empty( $_SESSION[$name.'_class'] ) ? $_SESSION[$name.'_class'] : 'success';
            echo '<div class="'.$class.'" id="msg-flash">'.$_SESSION[$name].'</div>';
            unset($_SESSION[$name]);
            unset($_SESSION[$name.'_class']);
        }
    }
}

}
?>