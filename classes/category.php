<?php 
/**
* @category class
* @author Paras Nath Chaudhary
* @version 1.0
*/

/**
* class Category
*/
class Category{

	private $id;
	private $title;
	private $description;
	private $image;
	private $status;
	private $timestamp;

	
	/**
	* Setter function  for id attribute
	* @param int $value	
	*/
    public function setId($value){
    	$this->id=$value;
    }
    /**
    * Getter function of id attribute
    * @return int
    */
    public function getId(){
    	return $this->id;
    }
    /**
	* Setter function  for title attribute
	* @param int $value	
	*/
    public function setTitle($value){
    	$this->title=$value;
    }
    /**
    * Getter function of title attribute
    * @return string
    */
    public function getTitle(){
    	return $this->title;
    }
    /**
	* Setter function  for description attribute
	* @param int $value	
	*/
    public function setDescription($value){
    	$this->description=$value;
    }
    /**
    * Getter function of description attribute
    * @return string
    */
    public function getDescription(){
    	return $this->description;
    }
    public function setImage($value){
    	$this->image=$value;
    }
    public function getImage(){
    	return $this->image;
    }
    public function setStatus($value){
    	$this->status=$value;
    }
    public function getStatus(){
    	return $this->status;
    }
    public function setTimestamp($value){
    	$this->timestamp=$value;
    }
    public function getTimestamp($value){
    	return $this->timestamp;
    }

    /**
    * getCategories function returns categories
    * @param int $limit, int $offset
    * @return Categories
    */
	public static function getCategories($limit=10,$offset=0){
		
		global $db;		
		$categories = $db->prepare("SELECT * FROM  `categories` order by timestamp desc LIMIT :offset , :limit");
		$categories->bindParam(':offset',$offset,PDO::PARAM_INT);
		$categories->bindParam(':limit',$limit,PDO::PARAM_INT);
		
		$categories->execute();

		return  $categories->fetchAll();	
	}
	/**
	* Saves category	
	*/
	public function save(){

		global $db;

		if(@$this->id){
			
			$category = $db->prepare("UPDATE categories set `title`=:title,`description`=:description,`image`=:image,`status`=:status,`timestamp`=:timestampvalue where id=:id");
			$category->bindParam(':id',$this->id,PDO::PARAM_INT);

		}else{

			$category = $db->prepare("INSERT INTO categories(`id`,`title`,`description`,`image`,`status`,`timestamp`)
					VALUES(NULL,:title,:description,:image,:status,:timestampvalue)");
		}
		

		$category->bindParam(':title',$this->title);
		$category->bindParam(':description',$this->description);
		$category->bindParam(':image',$this->image);
		$category->bindParam(':status',$this->status,PDO::PARAM_INT);
		$category->bindParam(':timestampvalue',$this->timestamp);

		$category->execute();
	}
	/**
	* Gets a category by its id
	* @param int $id
	* @return Category
	*/
	public static function getCategoryById($id=null){
		global $db;
		$category = $db->prepare("SELECT * FROM categories WHERE id=:id");
		$category->bindParam(':id',$id);
		$category->execute();
		//return $category->fetchAll();
		return $category->fetchAll(PDO::FETCH_CLASS, "Category");

	}
}
