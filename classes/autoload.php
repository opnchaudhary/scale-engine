<?php
	function __autoload($class_name) {

		if(file_exists(strtolower($class_name) . '.php')){

			include_once strtolower($class_name) . '.php';    		//includes files with lower case names

		}elseif(file_exists(ucfirst($class_name) . '.php')){
			include_once strtolower($class_name) . '.php';        //include files with first character in upper case
		}
    	
	}
	require_once 'csrf.php';
	require_once 'session.php';
	require_once 'category.php';
	require_once 'db.php';
	require_once 'config/database.php';
	require_once 'config/se.php';
?>