<?php 
class Db{

/*	public function __construct(){

		//try{
				$con = new PDO("mysql:host=".DB_HOST.";dbname=".DB_SCHEMA, DB_USER, DB_PASS,array(PDO::ATTR_PERSISTENT=>true));
				$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$con->exec("SET NAMES 'utf8'");
					
				return $con;

		/*	}catch(Exception $e){

				return false;

			}
	}*/

	public static function connect(){
		$con = new PDO("mysql:host=".DB_HOST.";dbname=".DB_SCHEMA, DB_USER, DB_PASS,array(PDO::ATTR_PERSISTENT=>true));
		$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$con->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ); 
		$con->exec("SET NAMES 'utf8'");
					
		return $con;

	}
	

}
?>