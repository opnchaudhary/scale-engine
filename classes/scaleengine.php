<?php 
/**
* @category class
* @author Paras Nath Chaudhary
* @version 1.0
*/

/**
* class ScaleEngine
*/

class Scaleengine{

	private $api_key;
	private $api_secret;
	private $api_url;
	private $api_dev_url;
	private $api_cdn;
	private $api_channel_id;

	private $ftp_username;
	private $ftp_password;
	private $ftp_host;


	/**
	* Adds video to a channel
	* @param 
	* @return 
	*/
	public function addVideo(){

	}

	/**
	* Updates video information in a channel
	* @param 
	* @return 
	*/
	public function updateVideo(){

	}

	/**
	* Deletes a video from a channel
	* @param 
	* @return 
	*/
	public function deleteVideo(){

	}

	/**
	* Get all videos of a channel
	* @param 
	* @return 
	*/
	public function getVideosByChannel($channel_id){

	}

	/**
	* Saves a channel
	* @param 
	* @return 
	*/
	public function saveChannel(){

	}
	/**
	* Get all channels
	* @param 
	* @return 
	*/
	public function getChannels(){

	}

	/**
	* Get Channel Details
	* @param 
	* @return 
	*/
	public function getChannelDetails($channel_id){

	}
}

?>