<?php 
class Csrf{
	public static function generateToken(){
		$randomValue=rand().time();
		$_SESSION['csrf_token']=$randomValue;
		return sha1($randomValue);
	}
}
?>