<?php 
	include_once 'start.php';
	$data 	=	array(	
						'base_url'=>BASE_URL,
						'videos'=>Video::getVideos()
					);

	echo $twig->render('@backend/videos.twig.html', $data);
?>