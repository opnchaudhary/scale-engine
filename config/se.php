<?php 

date_default_timezone_set('UTC');

/**
* ScaleEngine API configuration
*/
if(!defined('API_APP_ID'))
  define( 'API_APP_ID', '' );

if(!defined('API_KEY'))
	  define( 'API_KEY', '' );

if(!defined('API_SECRET'))
  define( 'API_SECRET', '' );

if(!defined('API_URL'))
  define( 'API_URL', 'https://api.scaleengine.net/stable/' );

if(!defined('API_DEV_URL'))
  define( 'API_DEV_URL', 'https://api.scaleengine.net/dev/' );

/**
* ScaleEngine FTP configuration
*/
if(!defined('FTP_HOST'))
  define( 'FTP_HOST', '' );

if(!defined('FTP_USER'))
  define( 'FTP_USER', '' );

if(!defined('FTP_PASS'))
  define( 'FTP_PASS', '' );
?>