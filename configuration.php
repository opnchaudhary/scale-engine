<?php
   include_once 'start.php';

	if(isset($_POST['save_config']) && $_POST['save_config']){

		echo "Updating configuration";

	}else{

		$csrf_token=Csrf::generateToken();

		$data 	=	array(
						'csrf_token'=>$csrf_token,
						'base_url'=>BASE_URL
					);

		echo $twig->render('@backend/config.twig.html', $data);
	}
?>