<?php 
	include_once 'start.php';
	
	

	if($loggedIn){

		if($_POST){

			$category=Category::getCategoryById($_GET['id']);
			if( !$category ){
				die('Category not found.');
			}
			$oldCategory=$category[0];
			
			$category=new Category();

			$category->setId($_POST['category_id']);
			$category->setTitle($_POST['category_title']);
			$category->setDescription($_POST['category_description']);
			
			if($_FILES['category_image']){
				$category->setImage( "" );
			}else{
				$category->setImage( $_POST['category_old_image']);	
			}
			
			$category->setTimestamp( date("Y-m-d H:i:s") );
			$category->setStatus($_POST['category_status']);
						
			$category->save();
			Session::flash('success_msg','Category Saved successfully');
			header('location:edit_category?id='.$_POST['category_id']);

		}else{

			$category=Category::getCategoryById($_GET['id']);
			if( !$category ){
				die('Category not found.');
			}
			$data 	=	array(						
							'base_url'=>BASE_URL,
							'category_id'=>$_GET['id'],
							'category'=>$category[0]
						);		
			
			echo $twig->render('@backend/edit_category.twig.html', $data);


		}

		
	}else{		
		$data 	=	array(						
						'base_url'=>BASE_URL
					);
		echo $twig->render('@backend/login.twig.html', $data);
		
	}
	
?>