<?php 

	if($_POST){
		$category=new Category();

		
		$category->setTitle($_POST['category_title']);
		$category->setDescription($_POST['category_description']);
		$category->setImage("");
		$category->setStatus($_POST['category_status']);
		$category->setTimestamp( date("Y-m-d H:i:s") );
		
		$category->save();
		Session::flash('success_msg','Category Saved successfully');
		header('location:create_category');

	}else{

		$csrf_token=Csrf::generateToken();
		$data 	=	array(	
							'csrf_token'=>$csrf_token,
							'base_url'=>BASE_URL,	
							'success_msg'=>Session::flash('success_msg'),
							'error_msg'=>Session::flash('error_msg'),
							'warning_msg'=>Session::flash('warning_msg'),
							'info_msg'=>Session::flash('info_msg')
						);

		echo $twig->render('@backend/create_category.twig.html', $data);

	}
	
?>