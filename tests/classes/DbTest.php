<?php
class DbTest extends PHPUnit_Framework_TestCase{

	protected function setUp(){
		if(!defined('BASE_PATH'))
            define('BASE_PATH','/var/www/html/scaleengine/');
        include_once BASE_PATH.'config/database.php';
        include_once BASE_PATH.'classes/db.php';
	}

    //Test for Generate Token function
    public function testConnect(){
        
        $db=Db::connect();
		$this->assertTrue($db);
    }
}
?>