<?php
class CsrfTest extends PHPUnit_Framework_TestCase{

	protected function setUp(){
		if(!defined('BASE_PATH'))
            define('BASE_PATH','/var/www/html/scaleengine/');
        include_once BASE_PATH.'classes/csrf.php';
	}

    //Test for Generate Token function
    public function testGenerateToken(){
        
        $csrf_token=Csrf::generateToken();
		$this->assertEquals(sha1($_SESSION['csrf_token']),$csrf_token);        
    }
}
?>