<?php
class UserTest extends PHPUnit_Framework_TestCase{

    protected function setUp(){
        if(!defined('BASE_PATH'))
            define('BASE_PATH','/var/www/html/scaleengine/');
        include_once BASE_PATH.'classes/user.php';
    }

    public function testFileExists(){

        $this->assertFileExists(BASE_PATH.'classes/user.php');

    }
    public function testAttributeExists(){

        $this->assertClassHasAttribute('username', 'User');    

    }
    
}
?>