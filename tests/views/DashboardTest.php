<?php
class DashboardTest extends PHPUnit_Extensions_SeleniumTestCase
{
    protected function setUp()
    {
        $this->setBrowser('firefox');
        $this->setBrowserUrl('http://localhost/scaleengine/');
    }

    public function testTitle(){

        $this->open('http://localhost/scaleengine/');        
        $this->assertTitle('Main Index Page');

    }

    public function testDashboard(){
    	/*
		$this->assertElementContainsText(
		    "//p[@id='foo']/a[@href='http://www.example.com/']",
		    'bar'
		);
    	*/

    	$this->open('http://localhost/scaleengine/');        

        $this->assertElementContainsText('//h1','Dashboard');

       // $this->assertElementContainsText('//p:first','Dashboard');
    }

}
?>